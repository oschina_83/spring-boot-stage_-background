
import router from '../router'
import axios from "axios";
export const CONTENT_TYPE = {
    formData: "application/x-www-form-urlencoded",
    jsonData: "application/json",
  };
//1.const request = axios.create({：创建一个axios实例并命名为request。
const request = axios.create({
    //设置基础URL，它将被添加到每个《请求》的URL前面
    baseURL: import.meta.env.VITE_BASE_URL,
    //在我的vue夹下的.env.development,如果后端换接口了就要换
    //前后端分离
    timeout: 30000  // 后台接口超时时间设置
})
// request 拦截器 2.使用请求拦截器，在《发送请求前》进行一些操作。
// 可以自请求发送前对请求做一些处理
request.interceptors.request.use(config => {
    config.headers['Content-Type'] = 'application/json;charset=utf-8';
    // 对请求头中所有的中文进行编码    
    for (const key in config.headers) {
        // 检查字段的值是否为字符串类型
        if (typeof config.headers[key] === 'string') {
            // 如果值中包含中文，则进行编码
            if (/[\u4e00-\u9fa5]/.test(config.headers[key])) {
                config.headers[key] = encodeURIComponent(config.headers[key]);
            }
        }
    }
    //设置所有请求的Content-Type。《返回配置》，
    if (config.url === '/login' && config.method === 'post') {
        config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    }
    if (
      config.method === "post" &&
      config.headers["Content-Type"] === CONTENT_TYPE.formData
    ) {
      let formdata = new FormData();
      Object.keys(config.data).forEach((key) => {
        formdata.append(key, config.data[key]);
      });
      config.data = formdata;
    }
    // get提交对参数进行编码
    if (config.method === "get" && config.params) {
      let url = config.url + "?";
      Object.keys(config.params).forEach((key) => {
        if (config.params[key] !== void 0 && config.params[key] !== null) {
          url += `${key}=${encodeURIComponent(config.params[key])}&`;
        }
      });
      url = url.substring(0, url.length - 1);
      config.params = {};
      config.url = url;
    }
    const token = JSON.parse(localStorage.getItem('Authentication'));
    if (token) {
      config.headers['Authentication'] = token;
    }
    return config;
  },
error => {
    return Promise.reject(error)
});
//3.如果没有错误，就《返回配置》，否则返回Promise.reject(error)，错误会在后续的catch方法中被捕获。

// response 拦截器
// 可以在接口响应后统一处理结果
// 4.使用《响应拦截器》，在《获取到响应后》进行一些操作。
request.interceptors.response.use(
    response => {
        let res = response.data;
        // 如果是返回的文件
        //如果响应类型是blob（一种表示大型二进制对象的数据类型），那么直接返回数据。
        if (response.config.responseType === 'blob') {
            return res
        }
        // 兼容服务端返回的字符串数据
        if (typeof res === 'string') {
            res = res ? JSON.parse(res) : res
        }
        // 当权限验证不通过的时候给出提示
        if (res.code === '401') {
            ElMessage.error(res.msg);
            router.push("/login")
        }
        //从《响应》中获取《数据》。
        return res;
    },
        error => {
        console.log('err' + error)
        return Promise.reject(error)
    }
)
export default request