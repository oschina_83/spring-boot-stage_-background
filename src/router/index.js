import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Manager',
      component: () => import('@/views/Manager.vue'),
      redirect: '/login',
      children: [
        { path:  'home', name: 'Home', component: () => import('@/views/manager/Home.vue')},
        { path:  'user', name: 'Course', component: () => import('@/views/manager/user.vue')},
        { path:  'auth',name: 'auth',component: () => import('@/views/auth/index.vue')},
        { path:  'role_auth',name: 'role_auth',component: () => import('@/views/role/role_auth.vue')},
        { path:  'user_role',name: 'user_role',component: () => import('@/views/role/user_role.vue')},
        { path:  'role',name: 'role',component: () => import('@/views/role/role.vue')},
        { path:  'notice',name: 'Notice',component: () => import('@/views/notice/index.vue')},
        { path: 'category', name: '商品分类管理', component: () => import('@/views/product/category.vue') },
        { path: 'product_details', name: '商品信息管理', component: () => import('@/views/product/product_details.vue') },
        { path: 'business', name: '商家管理', component: () => import('@/views/product/business.vue') },
        { path: 'inform', name: '举报管理', component: () => import('@/views/inform_appeal/inform.vue') },
        { path: 'appeal', name: '申诉管理', component: () => import('@/views/inform_appeal/appeal.vue') },
        { path: 'logistics', name: '物流管理', component: () => import('@/views/product/logistics.vue') }
      ]
    },


    {
      //@表示src
      path: '/login',
      name: 'Login',
      component: () => import('@/views/Login.vue'),
    },
    {
      //@表示src
      path: '/register',
      name: 'Register',
      component: () => import('@/views/register.vue'),
    }
  ]
})

export default router
