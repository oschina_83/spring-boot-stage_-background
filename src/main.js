import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import ElementPlus from 'element-plus'
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
//引入图形库
import '@/assets/css/global.css'
import {createPinia } from 'pinia'
import VueQuillEditor from 'vue-quill-editor'

// import styles for quill editor
import 'quill/dist/quill.core.css' // core style
import 'quill/dist/quill.snow.css' // snow theme
import 'quill/dist/quill.bubble.css' // bubble theme



const app = createApp(App)

app.use(createPinia)
app.use(VueQuillEditor)
app.use(router)
app.use(ElementPlus, {
    locale: zhCn,
})
app.mount('#app')

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}